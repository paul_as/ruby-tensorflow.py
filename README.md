# CV2

Ruby wrapper for the Python TensorFlow API using PyCall via RubyPy.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tensorflow.py'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tensorflow.py

## Usage

```ruby
require 'tensorflow'

tf = TensorFlow
sess   = tf.Session.new
a      = tf.placeholder tf.int16
b      = tf.placeholder tf.int16
add    = tf.add(a, b)
mul    = tf.multiply(a, b)
result = sess.run(add, feed_dict: { a => 2, b => 3 })
sess.close

# block sugar closes the session
tf.session do |sess|
  sess.run(mul, feed_dict: { a => 2, b => 3 })
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

https://gitlab.com/fjc/ruby-tensorflow.py

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
